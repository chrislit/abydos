# -*- coding: utf-8 -*-

from distutils.core import setup
setup(
    name = "abydos",
    packages = ["abydos"],
    version = "1.0.0",
    description = "Abydos NLP library",
    author = "Christopher C. Little",
    author_email = "chrisclittle@gmail.com",
    url = "https://github.com/chrislit/abydos",
    download_url = "https://github.com/chrislit/abydos/archive/master.zip",
    keywords = ["nlp", "ai"],
    classifiers = [
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Text Processing :: Indexing",
        "Topic :: Text Processing :: Linguistic",
        ],
    long_description = """\
Abydos NLP library
------------------

Includes ...

This version requires Python 2.7 or later.
"""
)
